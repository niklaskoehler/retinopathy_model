This is a TensorFlow example implementation for retina classification. The network is a standard residual network model with 50 layers. The model is defined in 'resnet_model' and 'resnet_run_loop.py'. The model is optimized for GPU usage and does currently not run on CPUs, due to the lack of pooling support for NCHW tensorformat.


## 1 Generating training record files
The folder ./records/ contains lists which are pointing towards kaggle data. The Kaggle data has to be preprocessed by resizing each image to 256x256 Pixels. 
```bash
python3 convert_to_tf.py
```
This call converts all images from the list into a tfrecord binary file which is optimized for serialized reading from disc. Those .tfrecords files are used by the training routine for training and evaluation of the model.

## 2 Training the model
to train the model call:
```bash
python3 retinopathy_main.py
```
the model parameters get frequently stored in ./retinopathy_model. A TF model consists of 4 files:<br>
model.ckpt-10.meta <br>
model.ckpt-10.index<br>
model.ckpt-10.data-00000-of-00001<br>
checkpoint<br>

After finnishing training the model gets dumped into the 'retinopathy_serve' folder. For first tests a set of trained parameters is already shared here: https://www.dropbox.com/sh/j7da25nymp141u7/AAAnhV8NdhoEsT-uA2TZuJHQa?dl=0



## 4 Predicting an image with the frozen graph
In order to perform example image classifcation the network, execute
```bash
python3 run_prediction.py --model_dir './retinopathy_serve/' --image_file dr4.tiff,healthy.tiff

dr4.tiff {'probabilities': array([[0.07751556, 0.0023793 , 0.02893336, 0.00789788, 0.88327396]],
      dtype=float32), 'classes': array([4])}
healthy.tiff {'probabilities': array([[0.8012319 , 0.07241397, 0.1168763 , 0.00747491, 0.00200295]],
      dtype=float32), 'classes': array([0])}

```




